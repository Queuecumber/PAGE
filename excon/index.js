/*
 The MIT License (MIT)

 Copyright (c) 2017 Max Ehrlich

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/
'use strict'

let express = require('express')
let app = express()
let experiment = require('./experiment')
let dns = require('dns')

app.use('/experiment', experiment)

let port = process.env.PORT_OVERRIDE ? process.env.PORT : 80

let start_server = () => {
    app.listen(port, () => {
        console.log(`Listening on port ${port}, access on port ${process.env.PORT || port}`)
    })
}

let try_dns = (err, address) => {
    if(!err) {
        console.log(`Resolved leader.mesos to ${address}`)
        start_server()
    } else {
        console.log(`Unable to resolve leader.mesos, trying again (${err})`)
        dns.resolve('leader.mesos','A', try_dns)
    }
}

console.log(`Waiting for DNS to propagate (detected servers ${dns.getServers()}`)
dns.resolve('leader.mesos','A', try_dns)
