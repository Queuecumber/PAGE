/*
 The MIT License (MIT)

 Copyright (c) 2017 Max Ehrlich

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */
'use strict'

let express = require('express')
let bodyParser = require('body-parser')
let uuid = require('uuid')
let request = require('request-promise-native')
let SSE = require('express-sse')
let EventSource = require('eventsource')

let framework_id = null
let es = null

let Experiment = class {
    constructor(options) {
        this.docker_image = options.docker_image
        this.id = uuid.v4()
        this.sse = new SSE()
        this.status = null
        
        this.resources = {
        	cpus: options.cpus,
        	ram: options.ram
        }

        console.log(`Experiment assigned id number ${this.id}`)
        es.addEventListener('status_update_event', e => { this.update_check(e) })

        this.run()
    }

    run() {
        let job_description = {
            id: this.id,
            cpus: this.resources.cpus,
            mem: this.resources.ram,
            disk: 0,
            gpus: 1,
            instances: 1,
            "constraints": [
                [
                    "master",
                    "UNLIKE",
                    "true"
                ]
            ],
            acceptedResourceRoles: [
                '*'
            ],
            container: {
                type: 'MESOS',
                "volumes": [
                    {
                        "containerPath": "/data",
                        "hostPath": "/shared-data",
                        "mode": "RW"
                    }
                ],
                docker: {
                    image: this.docker_image,
                    forcePullImage: true
                }
            }
        }

        request.put({
            uri: `http://leader.mesos:8080/v2/apps`,
            body: [job_description],
            json: true
        })

        console.log(`Starting job for experiment ${this.id}`)
    }

    update_check(e) {
        console.log(`Got update from marathon with ${e.data}`)
        let data = JSON.parse(e.data)

        if(data.appId === `/${this.id}`) {
            this.status_change(data)
        }
    }

    get_container() {
        request.get('http://leader.mesos:5050/tasks').then(res => {
            let tdata = JSON.parse(res)
            let task_info = tdata.tasks.find(t => t.id === this.task_id)

            this.container_id = task_info.statuses[0].container_status.container_id.value
            console.log(`Got container id ${this.container_id} for experiment ${this.id}`)

            this.watch(0, 'stdout')
            this.watch(0, 'stderr')
        })
    }

    get sandbox_directory() {
        return `/data/mesos/slaves/${this.agent_id}/frameworks/${framework_id}/executors/${this.task_id}/runs/${this.container_id}`
    }

    status_change(data) {
        console.log('Status update processing')
        this.status = data.taskStatus
        if(this.status === 'TASK_RUNNING') {
            this.agent_id = data.slaveId
            this.task_id = data.taskId
            this.agent_host = data.host

            console.log(`Task set running with (${this.agent_id},${this.task_id},${this.agent_host}) fetching container id`)

            this.get_container()
        } else if(this.status === 'TASK_FINISHED') {
            this.stop()
        }

        this.sse.send({status: this.status}, 'status_change', undefined)
    }

    watch(offset, file) {
        if(this.status === "TASK_RUNNING") {
            let read_files_uri = `http://${this.agent_host}:5051/files/read`
            request.get({
                uri: read_files_uri,
                qs: {
                    path: `${this.sandbox_directory}/${file}`,
                    offset: offset,
                    length: 5000
                }
            }).then(res => {
                if(!res) {
                    setTimeout(() => {
                        this.watch(offset, file)
                    }, 200)
                } else {
                    let file_data = JSON.parse(res)

                    console.log(file_data)
                    let block = file_data.data
                    let block_read_size = block.length

                    this.sse.send(block, `${file}_change`, undefined)

                    setTimeout(() => {
                        this.watch(offset + block_read_size, file)
                    }, 200)
                }
            }).catch(err => {
                console.log(`File watch emitted ${err}`)
                setTimeout(() => {
                    this.watch(offset, file)
                }, 200)
            })
        }
    }

    stop() {
        request.delete(`http://leader.mesos:8080/v2/apps/${this.id}`)
        es.removeEventListener('status_update_event', this.update_check)

        experiments.slice(experiments.indexOf(this), 1)
    }
}

let experiments = []

let router = express.Router()
router.use((req, res, next) => {
    if (es === null) {
        es = new EventSource('http://leader.mesos:8080/v2/events')
    }

    if (framework_id === null) {
        request.get('http://leader.mesos:8080/v2/info').then(r => {
            framework_id = JSON.parse(r).frameworkId
            console.log(`Found marathon with framework id ${framework_id}`)
            next()
        })
    } else {
        next()
    }
})

router.use(bodyParser.json())

router.get('/', (req, res) => {
    res.status(200).json(experiments)
})

router.post('/', (req, res) => {
    let experiment_proposal = req.body
    console.log(`Create experiment with ${experiment_proposal}`)

    let experiment = new Experiment(experiment_proposal)
    experiments.push(experiment)
    res.status(201).json(experiment)
})

router.get('/:id', (req, res) => {
    let experiment = experiments.find(e => e.id === req.params.id)

    if (experiment) {
        res.status(200).json(experiment)
    } else {
        res.status(404)
    }
})

router.delete('/:id', (req, res) => {
    // TODO delete experiment with id
    res.status(501)
})

router.get('/:id/connect', (req, res, next) => {
    let experiment = experiments.find(e => e.id === req.params.id)

    if (experiment) {
        console.log(`Connection for experiment ${experiment.id}`)
        experiment.sse.init(req, res, next)
    } else {
        res.status(404)
    }
})

module.exports = router
